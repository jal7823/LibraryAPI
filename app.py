#Libraries
import os
from flask import Flask,request,jsonify
from flask_restx import Resource,Api,fields,Namespace
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from dotenv import load_dotenv
from flask_login import LoginManager

login_manager = LoginManager()
load_dotenv()

#internals
from routes.route_libraries import library,libraries_ns
from routes.route_books import book,books_ns
from routes.route_login import login,login_ns
from utils.db import db
from models.model_books import Books
from models.model_users import Users

app = Flask(__name__)
migrate = Migrate(app,db)
# app.config['SQLALCHEMY_DATABASE_URI'] = f'mysql+pymysql://{os.getenv("DB_USER")}:{os.getenv("DB_PASS")}@{os.getenv("DB_HOST")}:/{os.getenv("DB_DATABASE")}'
app.config['SQLALCHEMY_DATABASE_URI'] = f'mysql+pymysql://pruebasJalMos:23051988joswelj@pruebasJalMos.mysql.pythonanywhere-services.com/pruebasJalMos$librariyDB'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_ECHO'] = True
db = SQLAlchemy(app)

db.init_app(app)
login_manager.init_app(app)
app.secret_key = 'SUPER SECRET'

authorizations = {
    'authorization': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'Authorization'
    },
}


api = Api(app,
    title='Api de Libraries',
    description='API for manage stock of book distributed in different libraries,in this API version you need logged and should be recieved a token to access for do a CRUD in the different endpoints',
    version='0.1.0',
    authorizations=authorizations,
    security='authorization',

)


app.register_blueprint(library, url_prefix='/library')   
app.register_blueprint(book, url_prefix='/books')   
app.register_blueprint(login, url_prefix='/login')   

api.add_namespace(libraries_ns)
api.add_namespace(books_ns)
api.add_namespace(login_ns)

@login_manager.user_loader
def load_user(id):
    return Users.query.get(id)

#database manager in shell
@app.shell_context_processor
def make_shell_context():
    return {
        'db':db,
        'Books':Books
    }