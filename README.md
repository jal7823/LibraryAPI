# API of Library

This is a simple API that manager the stock of books in different branch offices, with this API you can create,update,delete and read the books and libraries.

##  Stack of technologies
* Framework base
    * Flask
* Data base
    * Mysql
        *SQLAlchemy
* Documentation
    * Swagger
        * Flask restx

---
## Data base structure

### Books table
| Field  | Type         | Null | Key | Default | Extra          |
|--------|--------------|------|-----|---------|----------------|
| id     | int          | NO   | PRI | NULL    | auto_increment |
| title  | varchar(233) | YES  |     | NULL    |                |
| author | varchar(233) | YES  |     | NULL    |                |
-----------------------------------------------------------------

### Library table
| Field   | Type         | Null | Key | Default | Extra          |
|---------|--------------|------|-----|---------|----------------|
| id      | int          | NO   | PRI | NULL    | auto_increment |
| name    | varchar(100) | YES  |     | NULL    |                |
| address | varchar(255) | YES  |     | NULL    |                |
------------------------------------------------------------------


## :gear: Installation and configuration

### Environment

:warning: Is very important before install any package create a environment, you can use **virtualenv** or **anaconda**

##### Anaconda
```
conda activate nameofyourenvironent
```

##### virtualenv (Linux) 
```
source nameofyourenvironment/bin/activate
```

##### virtualenv (Windows) 
```
.\nameofyourenvironment\Script|activate
```
### :gear: Installed packages

All packages will be installed running the next command line

```
pip3 install -r requirements.txt
```

### :gear: Configure your URI (connection to Data Base)

Create a new file **.env** and set the following variables:

```
DB_USER
DB_PASS
DB_DATABASE
DB_HOST
```
This will configure automatically the variable include in the file **app.py**

```
app.config['SQLALCHEMY_DATABASE_URI'] = f'mysql+pymysql://{os.getenv("DB_USER")}:{os.getenv("DB_PASS")}@localhost/{os.getenv("DB_NAME")}'
```

### Run project

Now, you need run :

```
python3 index.py
```

Now, open this [link]('http://localhost:5000') and it will take you to the documentation.


## Quick start

This will be enabled soon through Docker
