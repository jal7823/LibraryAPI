from utils.db import db
from utils.ma import ma

class Library(db.Model):
    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    name = db.Column(db.String(100))
    address = db.Column(db.String(255))
    books = db.relationship('Books',backref='books')


class LibrarySchema(ma.Schema):
    class Meta:
        fields = ('id','name','address','books')

librarySchema = LibrarySchema()
librarySchemas = LibrarySchema(many = True)
