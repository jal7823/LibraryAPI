from flask_login import UserMixin
from utils.db import db
from utils.ma import ma

class Users(UserMixin,db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(255))
    password = db.Column(db.String(200))
    email = db.Column(db.String(100))
    role = db.Column(db.String(255))
    active = db.Column(db.Boolean())

    def __str__(self) -> str:
        return self.username

class userSchema(ma.Schema):
    class Meta:
        fields = ('id', 'name','username','email','password','active','role')

user_schema = userSchema()
user_schemas = userSchema(many=True)