from utils.db import db
from utils.ma import ma


class Books(db.Model):
    id = db.Column(db.Integer,primary_key=True,autoincrement=True)
    title = db.Column(db.String(233),)
    author = db.Column(db.String(233),)
    library_id = db.Column(db.Integer,db.ForeignKey('library.id'))

    def __repr__(self):
        return self.title

class BookSchema(ma.Schema):
    class Meta:
        fields = ('id','title','author','library_id')

bookSchema = BookSchema()
bookSchemas = BookSchema(many=True)