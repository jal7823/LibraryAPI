from werkzeug.security import generate_password_hash,check_password_hash



def generate_password(password):
    hash_password = generate_password_hash(password,method='pbkdf2:sha256')
    cut_password = hash_password
    return cut_password

def check_password(hash_password,password):
    check = check_password_hash(hash_password,password)
    return check


