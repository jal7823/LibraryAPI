# from flask import jsonify
# import os
import jwt
from jwt import decode,encode,exceptions
from functools import wraps
from flask import request

secret = 'SUPER SECRET'
def create_token(payload:dict,output:False):
    """Create a new token

    Args:
        payload (dict): all data to need encode
        output (Boolean): this will be output

    Returns:
        str: token
    """    
    try:
        if output:
            token = encode(payload,secret,algorithm='HS256')
            return token
        token = encode(payload,secret,algorithm='HS256')
    
    except exceptions.InvalidAlgorithmError as e:
        return e 

def validate_token(token:str):
    try:
        token = decode(token,secret,algorithms=['HS256'])
        try:
            if token:
                return True
            else:
                return False
        except exceptions.DecodeError as e:
            return e
    except exceptions.DecodeError as e:
            return e
    except exceptions.InvalidSignatureError as e:
        return e
    except exceptions.DecodeError as e:
        return e



def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):

        token = request.headers.get('Authorization')
        
        try:
            if 'Bearer' in token:
                token_validate = validate_token(token)
                
                if token_validate:
                                    
                    return f(*args, **kwargs)
                else:
                    return {'message':'Invalid token something is wrong.'}
            else:
                return {'message':'Please login first and save your token'}
        except Exception as e:
            return e

    return decorated