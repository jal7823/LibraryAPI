from flask import Blueprint,jsonify,request
from flask_restx import Api,Resource,fields
from flask_login import login_required


from models.model_libraries import Library,librarySchema
from utils.db import db

library = Blueprint('api', __name__)
api = Api(library)

#name space
libraries_ns = api.namespace('Libraries',description='CRUD for libraries')

#serializer
library_serializer = libraries_ns.model(
    'Libraries',
    {
        'id': fields.Integer,
        'name': fields.String,
        'address': fields.String,
    }
)

@libraries_ns.route('/')
class Libraries(Resource):
    @api.doc(response = {
            200: 'Success',
            403:'You must be logged in to the server (Unauthorized)',
            401:'You must be logged in to the server and have a token (Unauthorized)'
        },
        
    )
    @api.marshal_list_with(library_serializer,code=200,envelope='Libraries')
    def get(self):
        """Gel all Libraries

        Returns:
            JSON: Should return a list of all Libraries
        """        
        try:
            libraries = Library.query.all()            
            return libraries

        #eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN0cmluZyIsInBhc3N3b3JkIjoicGJrZGYyOnNoYTI1NjoyNjAwMDAkdkZWbTJIdmJ1WmRaTnhuciRhMDk0MGRmZTQ2ODQ3NTc4ODI3ZGMyYmM0MjkwNDc4MmZhOGEwN2RmZGJiZWJhNmJjNTJjNWRiOGFmMzQ5OThiIiwiZW1haWwiOiJzdHJpbmciLCJyb2xlIjoic3RyaW5nIiwiYWN0aXZlIjp0cnVlfQ.cxkGQK4G7Fpo90o41-Iiqm0-G_PYO1md2yQ3KwKHTJk
        except Exception as e:
            return e

    @api.marshal_list_with(library_serializer,code=201,envelope='Libraries')
    @login_required
    @api.doc(responses = {
            200: 'Success',
            403:'You must be logged in to the server (Unauthorized)',
            401:'You must be logged in to the server and have a token (Unauthorized)'
        }
    )
    @api.expect(library_serializer)
    def post(self):
        """Create a new Library

        Returns:
            JSON: Should return a new Library
        """        
        data = request.get_json()
        name = data.get('name')
        address = data.get('address')
        new_library = Library(name=name,address=address)
        db.session.add(new_library)
        db.session.commit()
        return Library.query.all()[-1]

@libraries_ns.route('/<int:id>')
class LibrariesResources(Resource):

    @api.marshal_with(library_serializer,code=200,envelope='Libraries')
    @login_required
    @api.doc(responses = {
            200: 'Success',
            403:'You must be logged in to the server (Unauthorized)',
            401:'You must be logged in to the server and have a token (Unauthorized)'
        }
    )
    @api.doc({'id':'An id to identifier'})
    def get(self,id):
        """Get one library

        Args:
            id (int): Identifier to library

        Returns:
            JSON: Should return library
        """        
        library = Library.query.get_or_404(id)
        return library
    
    @api.marshal_with(library_serializer,code=200,envelope='Libraries')
    @login_required
    @api.doc(responses = {
            200: 'Success',
            403:'You must be logged in to the server (Unauthorized)',
            401:'You must be logged in to the server and have a token (Unauthorized)'
        }
    )
    def put(self,id):
        """Update a library

        Args:
            id (int): Identifier of library to Update

        Returns:
            JSON: Should return a library updated 
        """        
        library = Library.query.get_or_404(id)
        data = request.get_json()
        name = data.get('name')
        address = data.get('address')
        library.name = name
        library.address = address
        db.session.commit()
        return library

    @api.marshal_with(library_serializer,code=200,envelope='Libraries')
    @login_required
    @api.doc(responses = {
            200: 'Success',
            403:'You must be logged in to the server (Unauthorized)',
            401:'You must be logged in to the server and have a token (Unauthorized)'
        },
    )
    def delete(self,id):
        """Delete a library

        Args:
            id (int): Identifier for deleted a library

        Returns:
            JSON: Should return a message 'Library deleted successfully'
        """        
        Library = Library.query.get_or_404(id)
        db.session.delete(library)
        return {'message': 'Library deleted successfully'}



