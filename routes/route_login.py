from flask import Blueprint,request,jsonify
from flask_restx import Api,Resource,fields
from flask_login import login_user,logout_user
from utils.jwt_manager import  create_token,validate_token
from flask_login import login_required

from models.model_users import Users
from utils.db import db
from utils.password_manager import generate_password,check_password

login = Blueprint('login',__name__)
api = Api(login)

login_ns = api.namespace('Register',description='Register to users')

#serializer
register_serializer = login_ns.model(
    'Register',
    {
        'username':fields.String,
        'role':fields.String,
        'email':fields.String,
        'password':fields.String,
        'active':fields.Boolean,
    }
)

login_serializer = login_ns.model(
    'Login',
    {
        'username': fields.String,
        'password': fields.String,
    }
)

@login_ns.route('/')
class Register(Resource):
    @api.marshal_with(register_serializer,code=201,envelope='Register')
    @api.expect(register_serializer)
    def post(self):
        """Register user in database and save to session

        Variables:
            data: All user data
            token: Token
            user: User
            password: Password
            role: Role
            active: status in database for logic elimination (default=True)
            user = User registered
            
        Returns:
            JSON: Users registered
        """        
        data = request.get_json()
        username = data.get('username')
        password = data.get('password')
        email = data.get('email')
        role = data.get('role')
        active = data.get('active')
        user = Users.query.filter_by(username=username).first()
        
        if user is None:
            hash_password = generate_password(password)
            user = Users(username=username,password=hash_password,email=email,role=role,active=active)
            db.session.add(user)
            db.session.commit()
            return Users.query.all()[-1]
        else:
            return {'message':'The user already exists.'}

@login_ns.route('/login')
class Login(Resource):
    @api.expect(login_serializer)
    def post(self):
        """Login Users

        Returns:
            JSON: User
        """        
        data = request.get_json()        
        username = data.get('username')
        password = data.get('password')
        user = Users.query.filter_by(username=username).first()
        
        if user is None:
            return {'message':'The user does not exist.'}
        else:
            password = check_password(user.password,password)        

            if password:
            
                payload = {
                    'username':user.username,
                    'password':user.password,
                    'email':user.email,
                    'role': user.role,
                    'active':user.active
                }
                token = create_token(payload,output=True)
                
            else:
                return {'message':'The password is incorrect.'}
            
            login_user(user)
            user_logged = Users.query.get_or_404(user.id)
            
            return jsonify({
                'user':user_logged.username,
                'token':token,
            })

    def get(self):
        """Logout a user
        """
        logout_user()
        return jsonify({'message':'you are logged out'})





            