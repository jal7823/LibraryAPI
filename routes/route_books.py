from flask import Blueprint, request
from flask_restx import Api, Resource, fields
from flask_login import login_required


from models.model_books import Books
from utils.db import db
from utils.jwt_manager import token_required


book = Blueprint('book',__name__)
api = Api(book)

books_ns = api.namespace('Books',description='CRUD for Books')

book_serializer = books_ns.model(
    'Books',
    {
        'id': fields.Integer(readOnly=True,description='ID',example=1),
        'title' : fields.String,
        'author' : fields.String,
        'library_id': fields.Integer

    }
)

@books_ns.route('/')
class Book(Resource):
    @api.marshal_list_with(book_serializer,code=200,envelope='Books')
    @login_required
    @api.doc(responses = {
            200: 'Success',
            403:'You must be logged in to the server (Unauthorized)',
            401:'You must be logged in to the server and have a token (Unauthorized)'
        },
        
    )
    def get(self):
        """get all books

        Returns:
            json: list all books
        """                
        book = Books.query.all()
        return book

    @api.marshal_list_with(book_serializer,code=201,envelope='Books')
    @login_required
    @api.doc(response = {
            200: 'Success',
            403:'You must be logged in to the server (Unauthorized)',
            401:'You must be logged in to the server and have a token (Unauthorized)'
        },
        
    )
    @api.expect(book_serializer)
    def post(self):
        """create a new Book

        Returns:
            json: the last books
        """        
        data = request.get_json()
        title = data.get('title')
        author = data.get('author')
        library_id = data.get('library_id')        
        new_book = Books(title=title,author=author,library_id=library_id)
        db.session.add(new_book)
        db.session.commit()
        last_book = Books.query.all()[-1]
        print(f'<========= last_book ============ {last_book} =====================>')
        
        return last_book

@books_ns.route('/<int:id>')
class BookResource(Resource):

    @api.marshal_with(book_serializer,code=200,envelope="Books")
    @login_required
    @api.doc(response = {
            200: 'Success',
            403:'You must be logged in to the server (Unauthorized)',
            401:'You must be logged in to the server and have a token (Unauthorized)'
        },
        
    )
    @api.doc({'id':'An identifier'})
    def get(self,id):
        """Get a book

        Args:
            id (str): identifier of book

        Returns:
            json: A book
        """        
        book = Books.query.get_or_404(id)

        
        return book

    @api.marshal_with(book_serializer,code=200,envelope="Books")
    @login_required
    @api.response(403,'You must be logged in to the server (Unauthorized)')
    @api.response(401,'You must be logged in to the server and have a token (Unauthorized)')
    @api.expect(book_serializer)
    def put(self,id):

        """Update a book

        Args:
            id (str): identifier of book

        Returns:
            json: Should by return a book updated
        """        
        book = Books.query.get_or_404(id)
        data =  request.get_json()
        book.title = data.get('title')
        book.author = data.get('author')
        db.session.commit()
        return book,200

    @api.marshal_with(book_serializer,code=203,envelope="Books")
    @login_required
    @api.doc(response = {
            200: 'Success',
            403:'You must be logged in to the server (Unauthorized)',
            401:'You must be logged in to the server and have a token (Unauthorized)'
        },
        
    )
    @api.doc({'id':'An identifier to delete'})
    def delete(self,id):
        """Delete a book

        Args:
            id (str): Identifier

        Returns:
            json: Book was deleted
        """        
        book = Books.query.get_or_404(id)
        db.session.delete(book)
        db.session.commit()
        return {'message':'book was deleted'}